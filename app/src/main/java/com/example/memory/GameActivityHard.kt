package com.example.memory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class GameActivityHard : AppCompatActivity(),  View.OnClickListener{
    private lateinit var moviments: TextView
    private lateinit var pauseBt: ImageView
    private lateinit var viewModel: GameViewModel
    private lateinit var carta1: ImageView
    private lateinit var carta2: ImageView
    private lateinit var carta3: ImageView
    private lateinit var carta4: ImageView
    private lateinit var carta5: ImageView
    private lateinit var carta6: ImageView
    private lateinit var carta7: ImageView
    private lateinit var carta8: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_hard)
        supportActionBar?.hide()

        val dif = intent.extras?.get("dificultat").toString()
        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)
        pauseBt = findViewById(R.id.pauseButton)
        moviments = findViewById(R.id.moviments_text)

        viewModel.setGame(dif)

        carta1 = findViewById(R.id.carta1)
        carta2 = findViewById(R.id.carta2)
        carta3 = findViewById(R.id.carta3)
        carta4 = findViewById(R.id.carta4)
        carta5 = findViewById(R.id.carta5)
        carta6 = findViewById(R.id.carta6)
        carta7 = findViewById(R.id.carta7)
        carta8 = findViewById(R.id.carta8)

        carta1.setOnClickListener(this)
        carta2.setOnClickListener(this)
        carta3.setOnClickListener(this)
        carta4.setOnClickListener(this)
        carta5.setOnClickListener(this)
        carta6.setOnClickListener(this)
        carta7.setOnClickListener(this)
        carta8.setOnClickListener(this)

        updateUI()
    }

    override fun onClick(v: View?) {
        when (v) {
            carta1 -> girarCarta(0, carta1)
            carta2 -> girarCarta(1, carta2)
            carta3 -> girarCarta(2, carta3)
            carta4 -> girarCarta(3, carta4)
            carta5 -> girarCarta(4, carta5)
            carta6 -> girarCarta(5, carta6)
            carta7 -> girarCarta(6, carta7)
            carta8 -> girarCarta(7, carta8)
        }
    }

    private fun girarCarta(idCarta: Int, carta: ImageView) {
        carta.setImageResource(viewModel.girarCarta(idCarta, carta))
        Handler().postDelayed({
            carta.visibility = viewModel.match(idCarta)
            carta.setImageResource(viewModel.reverse(idCarta))
            if (gameOver()){
                goToScore()
            }
            moviments.text = viewModel.getMoves()
        },viewModel.sleepTime())
    }

    private fun gameOver(): Boolean{
        return viewModel.gameIsOver()
    }
    private fun goToScore(){
        val intent = Intent(this, ScoreActivity::class.java)
        intent.putExtra("SCORE",viewModel.getScore())
        intent.putExtra("DIFICULTY", viewModel.dif)
        startActivity(intent)
    }

    private fun updateUI() {
        carta1.visibility = viewModel.estatCarta(0)
        carta2.visibility = viewModel.estatCarta(1)
        carta3.visibility = viewModel.estatCarta(2)
        carta4.visibility = viewModel.estatCarta(3)
        carta5.visibility = viewModel.estatCarta(4)
        carta6.visibility = viewModel.estatCarta(5)
        carta7.visibility = viewModel.estatCarta(6)
        carta8.visibility = viewModel.estatCarta(7)

        carta1.setImageResource(viewModel.imatgeCarta(0))
        carta2.setImageResource(viewModel.imatgeCarta(1))
        carta3.setImageResource(viewModel.imatgeCarta(2))
        carta4.setImageResource(viewModel.imatgeCarta(3))
        carta5.setImageResource(viewModel.imatgeCarta(4))
        carta6.setImageResource(viewModel.imatgeCarta(5))
        carta7.setImageResource(viewModel.imatgeCarta(6))
        carta8.setImageResource(viewModel.imatgeCarta(7))

        moviments.text = viewModel.getMoves()
    }
}