package com.example.memory

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    lateinit var spinner : Spinner
    lateinit var startButton : Button
    lateinit var helpButton: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        spinner = findViewById(R.id.difficultySpinner)
        startButton = findViewById(R.id.startButton)
        helpButton = findViewById(R.id.help_button)

        val arrayAdapter: ArrayAdapter<Any?> = ArrayAdapter<Any?>(this, R.layout.spinner_list, resources.getStringArray(R.array.difficulties_array))
        arrayAdapter.setDropDownViewResource(R.layout.spinner_list)
        spinner.adapter = arrayAdapter

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Com jugar")
        builder.setMessage(R.string.help_text)
        builder.setPositiveButton("Ok") { _: DialogInterface, _: Int -> }

        helpButton.setOnClickListener {
            builder.show()
        }

        startButton.setOnClickListener{
            val dificulty = spinner.selectedItem.toString()
            val intent:Intent = if (dificulty == "Easy"){
                Intent(this, GameActivity::class.java)
            } else {
                Intent(this, GameActivityHard::class.java)
            }

            intent.putExtra("dificultat",dificulty)

            startActivity(intent)
        }
    }
}