package com.example.memory

import android.view.View
import android.widget.ImageView
import androidx.lifecycle.ViewModel

class GameViewModel: ViewModel() {

    private var imatges = arrayListOf(
        R.drawable.plane___day,
        R.drawable.plane___day,
        R.drawable.airship___day,
        R.drawable.airship___day,
        R.drawable.ballon___day,
        R.drawable.ballon___day,
        R.drawable.cloud___day,
        R.drawable.cloud___day
    )

    private val imatgesEasy = arrayListOf(
        R.drawable.plane___day,
        R.drawable.plane___day,
        R.drawable.airship___day,
        R.drawable.airship___day,
        R.drawable.ballon___day,
        R.drawable.ballon___day,
    )

    private var cartes = mutableListOf<Carta>()

    private var nombreDeCartes = 8
    private var moviments = 0
    private lateinit var storedCard : ImageView
    private var storedId = -1

    var dif = "Hard" //default dificulty

    fun setGame(dificulty: String){
        if (dificulty == "Easy"){
            dif = dificulty
            imatges = imatgesEasy
            nombreDeCartes = 6
        }
        setDataModel()
    }

    private fun setDataModel() {
        imatges.shuffle()
        for (i in 0 until nombreDeCartes){
            cartes.add(Carta(i))
        }
    }

    fun girarCarta(i:Int, img:ImageView):Int{
        val c:Carta = cartes[i]
        if (!c.isTurned){
            c.up()
            moviments ++
            if(!matchTime()) {
                storedCard = img
                storedId = i
            }
        }
        return imatges[i]
    }

    fun reverse(i:Int): Int{
        if (matchTime()){
            storedCard.setImageResource(R.drawable.card_reverse)
            cartes[storedId].down()
            cartes[i].down()
            return R.drawable.card_reverse
        }
        return imatges[i]
    }

    private fun matchTime(): Boolean{
        return moviments %2 == 0
    }

    fun match(i:Int): Int{
        if(matchTime()) {
            if (imatges[i] == imatges[storedId]) {
                storedCard.visibility = View.INVISIBLE
                cartes[i].isDeleted = true
                cartes[storedId].isDeleted = true
                cartes[i]
                return View.INVISIBLE
            }
        }
        return View.VISIBLE
    }

     fun sleepTime(): Long{
        if (matchTime()) {
            return 750
        }
        return 0
    }

    fun gameIsOver(): Boolean{
        for (c in cartes){
            if (!c.isDeleted)
                return false
        }
        return true
    }

    fun getScore(): String{
        var score = 100 - (moviments - 2 - nombreDeCartes)*10
        if (score < 0)
            score = 0
        if (score > 100)
            score = 100
        return (score).toString()
    }

    fun estatCarta(i: Int): Int{
        return if (cartes[i].isDeleted)
            View.INVISIBLE
        else
            View.VISIBLE
    }

    fun imatgeCarta(i: Int): Int {
        return if(cartes[i].isTurned)
            imatges[i]
        else
            R.drawable.card_reverse
    }

    fun getMoves():String {
        return "Moviments: " + (moviments/2).toString()
    }
}