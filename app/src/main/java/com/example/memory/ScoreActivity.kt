package com.example.memory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class ScoreActivity : AppCompatActivity() {
    private lateinit var toMenu : android.widget.Button
    private lateinit var playAgain : android.widget.Button
    private lateinit var score : android.widget.TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)
        supportActionBar?.hide()

        toMenu = findViewById(R.id.toMenu)
        playAgain = findViewById(R.id.playAgain)
        score = findViewById(R.id.score)
        score.text = intent.getStringExtra("SCORE")

        toMenu.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        playAgain.setOnClickListener {
            val dificulty = intent.getStringExtra("DIFICULTY")
            val intent:Intent = if (dificulty == "Easy")
                Intent(this, GameActivity::class.java)
            else
                Intent(this, GameActivityHard::class.java)
            startActivity(intent)
        }
    }
}