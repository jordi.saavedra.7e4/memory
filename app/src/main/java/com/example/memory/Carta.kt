package com.example.memory


data class Carta(val id:Int) {
    var isTurned = false
    var isDeleted = false

    fun up(){
        isTurned = true
    }

    fun down(){
        isTurned = false
    }

}